# Setup

## Android 

A arm-pie version of tor like [tor-0.3.4.8.arm-pie](https://git.openprivacy.ca/openprivacy/buildfiles/raw/master/tor/tor-0.3.4.8.arm-pie) is required to be placed in `android/libs/armeabi-v7a` with the name `libtor.so`

# Deployment

## Android

    qtdeploy -docker build android
    adb install deploy/android/build-debug.apk
