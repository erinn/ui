package characters

import (
	"cwtch.im/cwtch/model"
	"cwtch.im/ui/go/gobjects"
	"cwtch.im/ui/go/the"
)

func CwtchListener(callback func(message *gobjects.Message), groupID string, channel chan model.Message) {
	for {
		m := <-channel

		name := m.PeerID
		if name == the.Peer.GetProfile().Onion {
			name = "me"
		} else {
			contact := the.Peer.GetContact(m.PeerID)
			if contact != nil {
				name, _ = contact.GetAttribute("name")
			} else {
				name = m.PeerID[:16] + "..."
			}
		}

		callback(&gobjects.Message{
			groupID,
			m.PeerID,
			name,
			m.Message,
			"",
			m.PeerID == the.Peer.GetProfile().Onion,
			0,
			m.Timestamp,
		})
	}
}
