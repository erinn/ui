package characters

import (
	"cwtch.im/ui/go/gobjects"
	"cwtch.im/ui/go/the"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"log"
)

func PostmanPat(messages chan gobjects.Letter) {
	postOffice := make(map[string]chan gobjects.Letter)

	for {
		m := <-messages

		_, found := postOffice[m.To]
		if !found {
			postOffice[m.To] = make(chan gobjects.Letter, 100)
			go andHisBlackAndWhiteCat(postOffice[m.To])
		}

		postOffice[m.To] <- m
	}
}

func andHisBlackAndWhiteCat(incomingMessages chan gobjects.Letter) {
	for {
		m := <-incomingMessages
		connection := the.Peer.PeerWithOnion(m.To)
		connection.WaitTilAuthenticated()
		connection.DoOnChannel("im.ricochet.chat", channels.Outbound, func(channel *channels.Channel) {
			chatchannel, ok := channel.Handler.(*channels.ChatChannel)
			if ok {
				log.Printf("Sending packet")
				the.AcknowledgementIDs[chatchannel.SendMessage(m.Message)] = m.MID
			}
		})
	}
}
