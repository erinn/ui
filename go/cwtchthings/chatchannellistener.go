package cwtchthings

import (
	"cwtch.im/ui/go/cwutil"
	"cwtch.im/ui/go/gobjects"
	"cwtch.im/ui/go/the"
	"git.openprivacy.ca/openprivacy/libricochet-go/application"
	"git.openprivacy.ca/openprivacy/libricochet-go/channels"
	"time"
)

type ChatChannelListener struct {
	rai          *application.ApplicationInstance
	ra           *application.RicochetApplication
	addMessage   func(*gobjects.Message)
	acknowledged func(uint)
}

func (this *ChatChannelListener) Init(rai *application.ApplicationInstance, ra *application.RicochetApplication, addMessage func(*gobjects.Message), acknowledged func(uint)) {
	this.rai = rai
	this.ra = ra
	this.addMessage = addMessage
	this.acknowledged = acknowledged
}

// We always want bidirectional chat channels
func (this *ChatChannelListener) OpenInbound() {
	outboutChatChannel := this.rai.Connection.Channel("im.ricochet.chat", channels.Outbound)
	if outboutChatChannel == nil {
		this.rai.Connection.Do(func() error {
			this.rai.Connection.RequestOpenChannel("im.ricochet.chat",
				&channels.ChatChannel{
					Handler: this,
				})
			return nil
		})
	}
}

func (this *ChatChannelListener) ChatMessage(messageID uint32, when time.Time, message string) bool {
	this.addMessage(&gobjects.Message{
		this.rai.RemoteHostname,
		this.rai.RemoteHostname,
		"",
		message,
		cwutil.RandomProfileImage(this.rai.RemoteHostname),
		false,
		int(messageID),
		when,
	})
	go func() { // TODO: this is probably no longer necessary. check later
		time.Sleep(time.Second)
		the.CwtchApp.SaveProfile(the.Peer)
	}()
	return true
}

func (this *ChatChannelListener) ChatMessageAck(messageID uint32, accepted bool) {
	this.acknowledged(the.AcknowledgementIDs[messageID])
}
