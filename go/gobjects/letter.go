package gobjects

// a Letter is a very simple message object passed to us from the UI
type Letter struct {
	To, Message string
	MID         uint
}
