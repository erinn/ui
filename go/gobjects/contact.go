package gobjects

type Contact struct {
	Handle      string
	DisplayName string
	Image       string
	Server      string
	Badge       int
	Status      int
	Trusted     bool
}
