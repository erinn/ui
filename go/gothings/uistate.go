package gothings

import (
	"cwtch.im/cwtch/model"
	"cwtch.im/ui/go/constants"
	"cwtch.im/ui/go/gobjects"
	"cwtch.im/ui/go/the"
	"encoding/base32"
	"log"
	"strings"
)

type InterfaceState struct {
	parentGcd *GrandCentralDispatcher
	contacts  map[string]*gobjects.Contact
	messages  map[string][]*gobjects.Message
}

func NewUIState(gcd *GrandCentralDispatcher) (uis InterfaceState) {
	uis = InterfaceState{gcd, make(map[string]*gobjects.Contact), make(map[string][]*gobjects.Message)}
	return
}

func (this *InterfaceState) AddContact(c *gobjects.Contact) {
	if len(c.Handle) == 32 { // ADD GROUP
		//TODO: we should handle group creation here too probably? the code for groups vs individuals is weird right now ^ea

		if _, found := this.contacts[c.Handle]; !found {
			this.contacts[c.Handle] = c
			this.parentGcd.AddContact(c.Handle, c.DisplayName, c.Image, c.Server, c.Badge, c.Status, c.Trusted)
		}

		return
	} else if len(c.Handle) != 56 {
		log.Printf("sorry, unable to handle AddContact(%v)", c.Handle)
		return
	}

	if the.Peer.GetContact(c.Handle) == nil {
		decodedPub, _ := base32.StdEncoding.DecodeString(strings.ToUpper(c.Handle))

		pp := &model.PublicProfile{Name: c.DisplayName, Ed25519PublicKey: decodedPub[:32], Trusted: c.Trusted, Blocked: false, Onion: c.Handle, Attributes: make(map[string]string)}
		pp.SetAttribute("name", c.DisplayName)
		the.Peer.GetProfile().Contacts[c.Handle] = pp
		if c.Trusted {
			the.Peer.GetProfile().TrustPeer(c.Handle)
		}
		the.CwtchApp.SaveProfile(the.Peer)
		go the.Peer.PeerWithOnion(c.Handle)
	}

	if _, found := this.contacts[c.Handle]; !found {
		this.contacts[c.Handle] = c
		this.parentGcd.AddContact(c.Handle, c.DisplayName, c.Image, c.Server, c.Badge, c.Status, c.Trusted)
	}

	the.CwtchApp.SaveProfile(the.Peer)
}

func (this *InterfaceState) GetContact(handle string) *gobjects.Contact {
	return this.contacts[handle]
}

func (this *InterfaceState) AddMessage(m *gobjects.Message) {
	_, found := this.contacts[m.Handle]
	if !found {
		this.AddContact(&gobjects.Contact{
			m.DisplayName,
			m.Image,
			m.Handle,
			"",
			1,
			0,
			false,
		})
	}

	c := the.Peer.GetContact(m.Handle)
	if c == nil {

	}

	_, found = this.messages[m.Handle]
	if !found {
		this.messages[m.Handle] = make([]*gobjects.Message, 0)
	}

	this.messages[m.Handle] = append(this.messages[m.Handle], m)

	if this.parentGcd.CurrentOpenConversation() == m.Handle {
		if m.FromMe {
			m.From = "me"
		}

		this.parentGcd.AppendMessage(m.Handle, m.From, m.DisplayName, m.Message, m.Image, uint(m.MessageID), m.FromMe, m.Timestamp.Format(constants.TIME_FORMAT))
	} else {
		c := this.GetContact(m.Handle)
		c.Badge++
		this.UpdateContact(c.Handle)
	}
	the.CwtchApp.SaveProfile(the.Peer)
}

func (this *InterfaceState) GetMessages(handle string) []*gobjects.Message {
	_, found := this.messages[handle]
	if !found {
		this.messages[handle] = make([]*gobjects.Message, 0)
	}
	return this.messages[handle]
}

func (this *InterfaceState) UpdateContact(handle string) {
	c, found := this.contacts[handle]
	if found {
		this.parentGcd.UpdateContact(c.Handle, c.DisplayName, c.Image, c.Server, c.Badge, c.Status, c.Trusted)
	}
}
