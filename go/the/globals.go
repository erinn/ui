package the

import (
	"cwtch.im/cwtch/app"
	libPeer "cwtch.im/cwtch/peer"
)

var CwtchApp app.Application
var Peer libPeer.CwtchPeer
var CwtchDir string
var AcknowledgementIDs map[uint32]uint
