import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11

import "fonts/Twemoji.js" as T
import "panes"
import "widgets"

Item {
	id: windowItem
	width: 525
	height: 500

	readonly property real ratio: height / width

	FontAwesome { // PRETTY BUTTON ICONS
		id: awesome
		resource: "qrc:/qml/fonts/fontawesome.ttf"
	}

	function parse(text, size) { // REPLACE EMOJI WITH <IMG> TAGS
		T.twemoji.base = "qrc:/qml/fonts/twemoji/"
		T.twemoji.ext = ".png"
		T.twemoji.size = "72x72"
		T.twemoji.className = "\" height=\""+size+"\" width=\""+size
		return T.twemoji.parse(text)
	}

	function restoreEmoji(text) { // REPLACE <IMG> TAGS WITH EMOJI
		var re = /<img src="qrc:\/qml\/fonts\/twemoji\/72x72\/([^"]*?)\.png" width="10" height="10" \/>/g
		var arr
		var newtext = text
		while (arr = re.exec(text)) {
			var pieces = arr[1].split("-")
			var replacement = ""
			for (var i = 0; i < pieces.length; i++) {
				replacement += T.twemoji.convert.fromCodePoint(pieces[i])
			}
			newtext = newtext.replace(arr[0], replacement)
		}
		return newtext
	}

	function scale() {
		return 0.1 + 2 * zoomSlider.value
	}


	/* Rectangle { // THE TOOLBAR
		id: toolbar
		anchors.top: parent.top
		anchors.left: parent.left
		width: ratio >= 0.92 ? parent.width : 70
		height: ratio >= 0.92 ? 70 : parent.height
		color: "#4B3557"

		GridLayout {
			width: parent.width
			height: parent.height
			columns: ratio >= 0.92 ? children.length : 1


			ContactPicture {
				Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
				source: "qrc:/qml/images/profiles/001-centaur.png"
				status: -2
			}

			ContactPicture {
				Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
				source: "qrc:/qml/images/profiles/002-kraken.png"
				status: -2
			}

			ContactPicture {
				Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
				source: "qrc:/qml/images/profiles/003-dinosaur.png"
				status: -2
			}

			ContactPicture {
				Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
				source: "qrc:/qml/images/profiles/004-tree-1.png"
				status: -2
			}

			ContactPicture {
				Layout.alignment: Qt.AlignVCenter | Qt.AlignHCenter
				source: "qrc:/qml/images/profiles/005-hand.png"
				status: -2
			}
		}
	}*/

	RowLayout { // CONTAINS EVERYTHING EXCEPT THE TOOLBAR
		/* anchors.left: ratio >= 0.92 ? parent.left : toolbar.right
		anchors.top: ratio >= 0.92 ? toolbar.bottom : parent.top
		anchors.right: parent.right
		anchors.bottom: parent.bottom */
		anchors.fill: parent
		spacing: 0


		Rectangle { // THE LEFT PANE WITH TOOLS AND CONTACTS
			color: "#D2C0DD"
			Layout.fillHeight: true
			Layout.minimumWidth: Layout.maximumWidth
			Layout.maximumWidth: theStack.pane == theStack.emptyPane ? parent.width : 200
			visible: (ratio <= 1.08 && windowItem.width >= 500) || theStack.pane == theStack.emptyPane


			ContactList{
				anchors.fill: parent
			}
		}

		Rectangle { // THE RIGHT PANE WHERE THE MESSAGES AND STuFF GO
			color: "#EEEEFF"
			Layout.fillWidth: true
			Layout.fillHeight: true


			StackLayout {
				id: theStack
				anchors.fill: parent
				currentIndex: 0

				property alias pane: theStack.currentIndex
				readonly property int emptyPane: 0
				readonly property int messagePane: 1
				readonly property int settingsPane: 2
				readonly property int userProfilePane: 3
				readonly property int groupProfilePane: 4
				readonly property int addGroupPane: 5


				Item {} // empty

				MessageList { // messagePane
					anchors.fill: parent
				}

				SettingsPane{}

				ColumnLayout { // userProfilePane
					anchors.fill: parent


					StackToolbar {
						text: "Settings for Sarah"
						aux.visible: false

						back.onClicked: theStack.pane = theStack.messagePane
					}

					Label { text: "per-user things like contact name and picture will be edited here" }
				}

				GroupSettingsPane{}

				AddGroupPane { anchors.fill: parent }
			}
		}
	}

	PropertyAnimation { id: anmPopup; easing.type: Easing.InQuart; duration: 7000; target: popup; property: "opacity"; to: 0; }

	Rectangle { // THE ERROR MESSAGE POPUP
		id: popup
		anchors.top: parent.top
		anchors.horizontalCenter: parent.horizontalCenter
		anchors.topMargin: 20
		width: lblPopup.width + 30
		height: lblPopup.height + 8
		color: "#000000"
		opacity: 0.5
		radius: 15
		visible: false


		Label {
			id: lblPopup
			anchors.horizontalCenter: parent.horizontalCenter
			anchors.verticalCenter: parent.verticalCenter
			font.pixelSize: 18
			color: "#FFFFFF"
		}
	}


	Connections { // POPUPS ARE INVOKED BY GO FUNCS
		target: gcd

		onInvokePopup: function(str) {
			lblPopup.text = str
			popup.opacity = 0.5
			popup.visible = true
			anmPopup.restart()
		}
	}
}