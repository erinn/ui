import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11

import "../widgets"

ColumnLayout { // settingsPane
	anchors.fill: parent


	StackToolbar {
		text: "Cwtch Settings"
		aux.visible: false
	}

	ScalingLabel {
		Layout.maximumWidth: parent.width
		text: "welcome to the global app settings page!"
	}

	Slider {
		id: zoomSlider
		from: 0.5
		to: 2.4
	}

	ScalingLabel {
		text: "Large text"
		size: 20
	}

	ScalingLabel{
		text: "Default size text, scale factor: " + zoomSlider.value
	}

	ScalingLabel {
		text: "Small text"
		size: 8
	}


	Component.onCompleted: {
		zoomSlider.value = Screen.pixelDensity / 3.2 // artistic license. set by erinn. fight me before changing
		if (zoomSlider.value < zoomSlider.from) zoomSlider.value = zoomSlider.from
		if (zoomSlider.value > zoomSlider.to) zoomSlider.value = zoomSlider.to
	}
}