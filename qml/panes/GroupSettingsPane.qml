import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11

import "../widgets"

ColumnLayout { // groupSettingsPane
	anchors.fill: parent


	StackToolbar {
		id: toolbar
		aux.visible: false
	}

	ScalingLabel {
		text: "Server:"
	}

	TextEdit {
		id: txtServer
		width: 100
	}

	ScalingLabel{
		text: "Group name:"
	}

	TextEdit {
		id: txtGroupName
		width: 100
	}

	ScalingLabel {
		text: "Invitation:"
	}

	TextEdit {
		id: txtInvitation
		width: 200
	}

	SimpleButton {
		icon: "regular/clipboard"
		text: "copy"

		onClicked: {
			gcd.popup("copied to clipboard!")
			txtInvitation.selectAll()
			txtInvitation.copy()
		}
	}


	Connections {
		target: gcd

		onSupplyGroupSettings: function(name, server, invite) {
			toolbar.text = name
			txtGroupName.text = name
			txtServer.text = server
			txtInvitation.text = invite
		}
	}
}