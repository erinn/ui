import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11

import "../widgets"

ColumnLayout { // settingsPane
	anchors.fill: parent


	StackToolbar {
		text: "Create a new group"
		aux.visible: false
	}

	ScalingLabel {
		text: "Server:"
	}

	TextEdit {
		id: txtServer
		width: 100
		text: "6xnl4rlc5gq5crnyki6wp6qy2dy2brsjtfixwhgujfycsu7jly7pmxad"
	}

	ScalingLabel{
		text: "Group name:"
	}

	TextEdit {
		id: txtGroupName
		width: 100
		text: "my awesome group"
	}

	SimpleButton {
		text: "create"

		onClicked: {
			gcd.createGroup(txtServer.text, txtGroupName.text)
		}
	}


	Component.onCompleted: {
		zoomSlider.value = Screen.pixelDensity / 3.2 // artistic license. set by erinn. fight me before changing
		if (zoomSlider.value < zoomSlider.from) zoomSlider.value = zoomSlider.from
		if (zoomSlider.value > zoomSlider.to) zoomSlider.value = zoomSlider.to
	}
}