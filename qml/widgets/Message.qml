import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

import "controls" as Awesome

RowLayout {
	id: root
	//Layout.alignment: from == "me" ? Qt.AlignRight : Qt.AlignLeft
	anchors.left: from == "me" ? undefined : parent.left
	anchors.right: from == "me" ? parent.right : undefined
	height: Math.max(imgProfile.height, rectMessageBubble.height)

	property alias message: lbl.text
	property string from
	property string handle
	property string displayName
	property int messageID
	property bool fromMe
	property alias timestamp: ts.text
	property alias image: imgProfile.source
	property alias status: imgProfile.status


	Connections {
		target: gcd

		onAcknowledged: function(mid) {
			if (mid == messageID) {
				ack.source = "qrc:/qml/images/fontawesome/regular/check-circle.svg"
			}
		}
	}


	ContactPicture {
		id: imgProfile
		anchors.left: parent.left
		visible: from != "me"
	}

	Rectangle { // THIS IS JUST A PRETTY MESSAGE-HOLDING RECTANGLE
		id: rectMessageBubble
		height: lbl.height + ts.height + 8
		Layout.minimumHeight: height
		Layout.maximumHeight: height
		width: colMessageBubble.width + 6
		Layout.minimumWidth: width
		Layout.maximumWidth: width
		color: from == "me" ? "#B09CBC" : "#4B3557"
		radius: 5

		// the console will complain constantly about me setting these anchors, but qt only allows margins if they've been set to something
		// a kludge to fix this would be to have spacers before/after and set the widths according to the side they're on ^ea
		anchors.left: from == "me" ? undefined : imgProfile.right //parent.left
		anchors.right: from == "me" ? parent.right : undefined
		anchors.leftMargin: 5
		anchors.rightMargin: 9
		anchors.topMargin: 5


		ColumnLayout {
			id: colMessageBubble


			Column { // combine these into one element or else childrenRect won't play nicely
				TextEdit { // this is used as a helper to calculate the message box width
					id: dummy
					visible: false
					padding: 6
					leftPadding: 10
					font.pixelSize: 12
					wrapMode: TextEdit.NoWrap
					text: message
					textFormat: Text.RichText
				}

				TextEdit { // this is the actual text display
					id: lbl
					color: "#FFFFFF"
					padding: 6
					leftPadding: 10
					font.pixelSize: 12
					selectByMouse: true
					readOnly: true
					width: Math.min(dummy.width, root.parent.width - (imgProfile.visible ? imgProfile.width : 0) - 40)
					wrapMode: TextEdit.Wrap
					textFormat: Text.RichText
				}
			}

			RowLayout {
				id: rowBottom
				anchors.left: parent.left
				anchors.right: parent.right


				Label { // TIMESTAMP
					id: ts
					color: "#FFFFFF"
					font.pixelSize: 10
					anchors.left: parent.left
					leftPadding: 10
				}

				Label { // DISPLAY NAME FOR GROUPS
					color: "#FFFFFF"
					font.pixelSize: 10
					anchors.right: parent.right
					text: displayName
					visible: from != "me"
				}

				Image { // ACKNOWLEDGEMENT ICON
					id: ack
					anchors.right: parent.right
					source: displayName == "" ? "qrc:/qml/images/fontawesome/regular/hourglass.svg" : "qrc:/qml/images/fontawesome/regular/check-circle.svg"
					height: 10
					sourceSize.height: 10
					visible: from == "me"
				}
			}
		}
	}
}