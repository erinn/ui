import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3

import "controls" as Awesome
import "../fonts/Twemoji.js" as T

Rectangle {
    id: button
    width: (text == undefined || text == "" ? 0 : buttonText.width) + (icon == undefined || icon == "" ? 0 : ico.width) + 24 * gcd.themeScale
    Layout.minimumWidth: width
    Layout.maximumWidth: width
    height: 20 * gcd.themeScale
    Layout.minimumHeight: height
    Layout.maximumHeight: height
	color: mousedown ? "#B09CBC" : "#4B3557"
	border.color: "#4B3557"
	border.width: 1
    radius: 2
    antialiasing: true

    property bool checked: false
    property alias text: buttonText.text
    property alias font: buttonText.font.family
    property string icon
    property bool mousedown
    signal clicked


	RowLayout {
		anchors.centerIn: parent

		Image {
			anchors.left: parent.left
			id: ico
			source: "qrc:/qml/images/fontawesome/"+icon+".svg"
			height: button.height / 2
			sourceSize.height: button.height / 2
		}

		Label {
			id: buttonText
			font.pixelSize: button.height / 2
			color: "#FFFFFF"
			anchors.left: ico.right
			anchors.leftMargin: 6
			visible: button.text != "" && button.text != undefined
		}
	}


    MouseArea {
        id: mouseArea
        anchors.fill: parent

        onClicked: {
        	parent.focus = true
        	parent.clicked()
        }

       	onPressed: mousedown = true

       	onReleased: mousedown = false
    }

    Keys.onSpacePressed: clicked()
}