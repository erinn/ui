import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Controls.Styles 1.4
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11

ColumnLayout {
	id: root
	anchors.fill: parent
	width: parent.width

	property alias image: imgProfileImg.source
	property string nick
	property string onion


	Item{ height: 6 }

	Item { // PROFILE IMAGE
		id: imgProfile
		implicitWidth: 96
		implicitHeight: 96
		anchors.horizontalCenter: parent.horizontalCenter

		Rectangle { // WHITE CIRCLE BORDER
			width: 96
			height: 96
			color: "#FFFFFF"
			radius: width / 2

			Image { // ACTUAL IMAGE
				id: imgProfileImg
				anchors.fill: parent
				fillMode: Image.PreserveAspectFit
				visible: false
			}

			Image { // INNER CIRCLE MASK
				id: mask
				fillMode: Image.PreserveAspectFit
				visible: false
				source: "qrc:/qml/images/extra/clipcircle.png"
			}

			OpacityMask { // WE PUT IT ALL TOGETHER ANNND
				anchors.fill: imgProfileImg
				source: imgProfileImg
				maskSource: mask
			}


			Rectangle { // TOR STATUS INDICATOR
				color: "#FFFFFF"
				width: 12
				height: 12
				radius: 3
				anchors.right: parent.right
				anchors.bottom: parent.bottom
				anchors.margins: 8


				Rectangle { //0: no tor, 1: progress 0%, 2: progress 1-99%, 3: DONE
					id: rectTorStatus
					color: code == 3 ? "green": code == 2 ? "orange" : code == 1 ? "yellow" : "red"
					width: 8
					height: 8
					radius: 2
					anchors.right: parent.right
					anchors.bottom: parent.bottom
					anchors.margins: 2

					property int code
					property string message
					property bool hovered


					MouseArea {
						anchors.fill: parent
						hoverEnabled: true

						onEntered: rectTorStatus.hovered = true

						onExited: rectTorStatus.hovered = false
					}


					ToolTip.visible: hovered
					ToolTip.delay: 400
					ToolTip.timeout: 5000
					ToolTip.text: message
				}
			}
		}
	}

	//InplaceEditText { // USER NICKNAME
	//	id: lblNick
	//	anchors.horizontalCenter: parent.horizontalCenter
	//	width: parent.width
	//
	//	onUpdated: {
	//		gcd.updateNick(lblNick.text)
	//	}
	//}

	Text {
		id: txtNick
		fontSizeMode: Text.HorizontalFit
		minimumPixelSize: 10
		font.pixelSize: 20
		width: 195
		anchors.horizontalCenter: parent.horizontalCenter
		text: cbNick.editText

		MouseArea {
			anchors.fill: parent
			onClicked: {
				parent.visible = false
				cbNick.visible = true
			}
		}
	}

	ComboBox { // USER NICKNAME
		id: cbNick
		anchors.horizontalCenter: parent.horizontalCenter
		popup.font.pixelSize: 12
		width: 200
		font.pixelSize: 20
		model: ["erinn", "erinn (open privacy)", "supergirl", "add new profile..."]
		visible: false

		onCurrentTextChanged: {
			visible = false
			txtNick.visible = true
		}
	}

	Label { // ONION ADDRESS
		id: lblOnion
		font.pixelSize: 6
		Layout.fillWidth: true
		padding: 3
		horizontalAlignment: Text.AlignHCenter
		text: "This is your address. You can give it out to people!\n" + onion
	}

	Row { // TOOLS FOR EDITING PROFILE
		anchors.horizontalCenter: parent.horizontalCenter
		spacing: gcd.themeScale * 2


		TextEdit { // USED TO POWER THE COPY/PASTE BUTTON
			id: txtHidden
			visible: false
		}

		SimpleButton { // COPY ONION ADDRESS BUTTON
			icon: "regular/clipboard"
			text: "copy"

			onClicked: {
				gcd.popup("copied to clipboard!")
				txtHidden.text = nick.replace(" ", "~") + "~" + onion
				txtHidden.selectAll()
				txtHidden.copy()
			}
		}

		SimpleButton { // SETTINGS BUTTON
			icon: "solid/cog"

			onClicked: theStack.pane = theStack.settingsPane
		}

		SimpleButton { // SIGN OUT BUTTON
			icon: "solid/sign-out-alt"

			onClicked: {
				gcd.popup("not yet implemented, sorry :(")
			}
		}
	}

	Row {
		anchors.horizontalCenter: parent.horizontalCenter
		spacing: gcd.themeScale * 2


		SimpleButton { // CREATE GROUP BUTTON
			icon: "regular/clipboard"
			text: "new group"

			onClicked: theStack.pane = theStack.addGroupPane
		}
	}

	RowLayout {
		anchors.left: parent.left
		anchors.right: parent.right


		Rectangle { // ADD CONTACTS TEXTFIELD
			width: parent.width - 4
			height: 20
			color: "#EDEDED"
			border.color: "#AAAAAA"
			border.width: 1
			radius: 10
			anchors.horizontalCenter: parent.horizontalCenter


			TextEdit {
				property string hint: "... paste an address here to add a contact ..."
				anchors.verticalCenter: parent.verticalCenter
				anchors.horizontalCenter: parent.horizontalCenter
				font.pixelSize: 9
				color: "#888888"
				padding: 2
				text: hint
				selectByMouse: true

				onTextChanged: {
					if (text != hint && text != "") {
						console.log("to handle: "+text)
						gcd.importString(text)
						text = hint
					}
				}

				onFocusChanged: {
					text = focus ? "" : hint
				}

				onCursorPositionChanged: {
					text = focus ? "" : hint
				}
			}
		}
	}

	Connections {
		target: gcd

		onUpdateMyProfile: function(_nick, _onion, _image) {
			nick = _nick
			onion = _onion
			image = _image
		}

		onTorStatus: function(code, str) {
			rectTorStatus.code = code
			rectTorStatus.message = str
		}
	}
}