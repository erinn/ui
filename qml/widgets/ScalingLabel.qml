import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import QtQuick.Window 2.11


Label {
	font.pixelSize: gcd.themeScale * size
	wrapMode: Text.WordWrap

	property real size: 12
}