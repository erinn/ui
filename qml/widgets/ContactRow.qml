import QtGraphicalEffects 1.0
import QtQuick 2.7
import QtQuick.Controls 2.4
import QtQuick.Controls.Material 2.0
import QtQuick.Layouts 1.3
import CustomQmlTypes 1.0

RowLayout { // LOTS OF NESTING TO DEAL WITH QT WEIRDNESS, SORRY
	anchors.left: parent.left
	anchors.right: parent.right
	visible: true

	property alias displayName: cn.text
	property alias image: imgProfile.source
	property string handle
	property int badge
	property bool isActive
	property bool isHover
	property bool trusted
	property alias status: imgProfile.status
	property string server


	Rectangle { // CONTACT ENTRY BACKGROUND COLOR
		id: root
		anchors.left: parent.left
		anchors.right: parent.right
		height: childrenRect.height + 3
		width: parent.width
		color: isHover ? "#D2D2F3" : (isActive ? "#D2D2F3" : "#D2C0DD")


		RowLayout {
			width: parent.width
			anchors.left: parent.left
			anchors.right: parent.right


			ContactPicture {
				id: imgProfile
			}

			Label { // CONTACT NAME
				id: cn
				leftPadding: 10
				rightPadding: 10
				//wrapMode: Text.WordWrap
				anchors.left: imgProfile.right
				anchors.right: rectUnread.left
				font.pixelSize: 16
				font.italic: !trusted
				textFormat: Text.PlainText
				//fontSizeMode: Text.HorizontalFit
				elide: Text.ElideRight
			}

			Rectangle { // UNREAD MESSAGES?
				id: rectUnread
				anchors.right: parent.right
				height: 16
				width: lblUnread.width + 10
				radius: 8
				color: "#4B3557"
				visible: badge != 0
				anchors.rightMargin: 9


				Label {
					id: lblUnread
					anchors.verticalCenter: parent.verticalCenter
					anchors.horizontalCenter: parent.horizontalCenter
					color: "#FFFFFF"
					font.pixelSize: 12
					text: badge
				}
			}
		}
	}

	MouseArea {  // ONCLICK: LOAD CONVERSATION WITH THIS CONTACT
		anchors.fill: parent
		hoverEnabled: true

		onClicked: {
			gcd.broadcast("ResetMessagePane")
			isActive = true
			theStack.pane = theStack.messagePane
			gcd.loadMessagesPane(handle)
		}

		onEntered: {
			isHover = true
		}

		onExited: {
			isHover = false
		}
	}

	Connections { // UPDATE UNREAD MESSAGES COUNTER
		target: gcd

		onResetMessagePane: function() {
			isActive = false
		}

		onUpdateContact: function(_handle, _displayName, _image, _server, _badge, _status, _trusted) {
			if (handle == _handle) {
				displayName = _displayName
				image = _image
				server = _server
				badge = _badge
				status = _status
				trusted = _trusted
			}
		}
	}
}